class ConsultorsController < ApplicationController
  before_action :set_consultor, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /consultors
  # GET /consultors.json
  def index
    @consultors = Consultor.all
  end

  # GET /consultors/1
  # GET /consultors/1.json
  def show
    @rede = @consultor.rede
    @empresas = @consultor.empresas
  end

  # GET /consultors/new
  def new
    @consultor = Consultor.new
    @todas_empresas = Empresa.all
    @todas_redes = Rede.all
    @consultor_empresas = @consultor.empresas.build
  end

  # GET /consultors/1/edit
  def edit
    @todas_redes = Rede.all
  end

  # POST /consultors
  # POST /consultors.json
  def create
    @consultor = Consultor.new(consultor_params)
    @ems = Array.new
    if params != nil
      @p = params[:empresas]
      if @p != nil
        @p[:id].each do |empresa|
        
          @ems << Empresa.find(empresa) if !empresa.empty?
        
        end
      end
    end    
    
    @consultor.empresas = @ems if @ems != nil

    respond_to do |format|
      if @consultor.save
        @nome = @consultor.nome
        format.html { redirect_to @consultor, notice: "O Consultor: #{@nome} foi criado com sucesso." }
        format.json { render :show, status: :created, location: @consultor }
      else
        format.html { render :new }
        format.json { render json: @consultor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /consultors/1
  # PATCH/PUT /consultors/1.json
  def update
    respond_to do |format|
      if @consultor.update(consultor_params)
        @nome = @consultor.nome
        format.html { redirect_to @consultor, notice: "O Consultor: #{@nome} foi editado com sucesso." }
        format.json { render :show, status: :ok, location: @consultor }
      else
        format.html { render :edit }
        format.json { render json: @consultor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consultors/1
  # DELETE /consultors/1.json
  def destroy
    @nome = @consultor.nome
    @consultor.destroy
    respond_to do |format|
      format.html { redirect_to consultors_url, notice: "O Consultor: #{@nome} foi deletado." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consultor
      @consultor = Consultor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def consultor_params
      params.require(:consultor).permit(:nome, :cpf, :rede_id, :empresas)
    end
end
