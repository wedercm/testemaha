class EmpresasController < ApplicationController
  before_action :set_empresa, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /empresas
  # GET /empresas.json
  def index
    @empresas = Empresa.all
  end

  # GET /empresas/1
  # GET /empresas/1.json
  def show
    @consultores = @empresa.consultors
  end

  # GET /empresas/new
  def new
    @empresa = Empresa.new
    @todos_consultores = Consultor.all
    @empresa_consultores = @empresa.consultors.build
  end

  # GET /empresas/1/edit
  def edit
  end

  # POST /empresas
  # POST /empresas.json
  def create
    @empresa = Empresa.new(empresa_params)
    @cons = Array.new
    if params != nil
      @x = params[:consultors]
      if @x != nil
        @x[:id].each do |consultor|
        
          @cons << Consultor.find(consultor) if !consultor.empty?
        
        end
      end
    end    
    
    @empresa.consultors = @cons if @cons != nil

    respond_to do |format|
      if @empresa.save
        @nome = @empresa.nome
        format.html { redirect_to @empresa, notice: "A Empresa: #{@nome} foi criada com sucesso." }
        format.json { render :show, status: :created, location: @empresa }
      else
        format.html { render :new }
        format.json { render json: @empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /empresas/1
  # PATCH/PUT /empresas/1.json
  def update
    respond_to do |format|
      if @empresa.update(empresa_params)
        @nome = @empresa.nome
        format.html { redirect_to @empresa, notice: "A Empresa: #{@nome} foi editada com sucesso." }
        format.json { render :show, status: :ok, location: @empresa }
      else
        format.html { render :edit }
        format.json { render json: @empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empresas/1
  # DELETE /empresas/1.json
  def destroy
    @nome = @empresa.nome
    @empresa.destroy    
    respond_to do |format|
      format.html { redirect_to empresas_url, notice: "A Empresa: #{@nome} foi deletada."  }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_empresa
      @empresa = Empresa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def empresa_params
      params.require(:empresa).permit(:nome, :cnpj, :venda, :gasto)
    end
end
