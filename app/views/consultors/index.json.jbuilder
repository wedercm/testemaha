json.array!(@consultors) do |consultor|
  json.extract! consultor, :id, :nome, :cpf, :rede, :empresas
  json.url consultor_url(consultor, format: :json)
end
