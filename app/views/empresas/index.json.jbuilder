json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nome, :cnpj, :venda, :gasto, :consultors
  json.url empresa_url(empresa, format: :json)
end
