class Rede < ActiveRecord::Base
	 validates :nome, presence: true, uniqueness: true
	has_many :consultors
	has_many :empresas, :through => :consultors
end
