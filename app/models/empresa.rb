class Empresa < ActiveRecord::Base
	 validates :nome, presence: true
	 validates :cnpj, presence: true, uniqueness: true
	has_many :parts, dependent: :destroy
	has_many :consultors, :through => :parts
end
