class Consultor < ActiveRecord::Base
	validates :nome, presence: true
	validates :cpf, presence: true, uniqueness: true
	belongs_to :rede
	has_many :parts, dependent: :destroy
	has_many :empresas, :through => :parts
end
