# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
	r1 = Rede.create(nome: "Rede 1")
	r2 = Rede.create(nome: "Rede 2")

	c1 = Consultor.create(nome: "Consultor 1", cpf: "123", rede_id: r1.id)
	c2 = Consultor.create(nome: "Consultor 2", cpf: "456", rede_id: r1.id)
	c3 = Consultor.create(nome: "Consultor 3", cpf: "789", rede_id: r1.id)

	c4 = Consultor.create(nome: "Consultor 4", cpf: "321", rede_id: r2.id)
	c5 = Consultor.create(nome: "Consultor 5", cpf: "654", rede_id: r2.id)
	c6 = Consultor.create(nome: "Consultor 6", cpf: "987", rede_id: r2.id)

	e1 = Empresa.create(nome: "Empresa 1", cnpj: "1010", venda: 1_000)
	e2 = Empresa.create(nome: "Empresa 2", cnpj: "1011", venda: 2_000)
	e3 = Empresa.create(nome: "Empresa 3", cnpj: "1012", venda: 4_000)
	e4 = Empresa.create(nome: "Empresa 4", cnpj: "1013", venda: 5_000)
	e5 = Empresa.create(nome: "Empresa 5", cnpj: "1014", gasto: 1_000)
	e6 = Empresa.create(nome: "Empresa 6", cnpj: "1015", gasto: 2_000)
	e7 = Empresa.create(nome: "Empresa 7", cnpj: "1016", gasto: 3_000)
	e8 = Empresa.create(nome: "Empresa 8", cnpj: "1017", gasto: 4_000)

	e1.consultors << [c1,c3,c5]
	e2.consultors << [c2,c4,c6]
	e3.consultors << [c1,c2,c3,c4,c5,c6]
	e3.consultors << [c1,c2,c5,c6]