class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
   		t.belongs_to :consultor, index: true
    	t.belongs_to :empresa, index: true
      	t.timestamps null: false
    end
  end
end
