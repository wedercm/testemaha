class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nome, null: false
      t.string :cnpj, null: false, unique: true
      t.float :venda, default: 0.0
      t.float :gasto, default: 0.0

      t.timestamps null: false
    end
  end
end
