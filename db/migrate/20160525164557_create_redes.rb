class CreateRedes < ActiveRecord::Migration
  def change
    create_table :redes do |t|
      t.string :nome, null: false, unique: true
      t.timestamps null: false
    end
  end
end
