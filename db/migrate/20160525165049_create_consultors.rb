class CreateConsultors < ActiveRecord::Migration
  def change
    create_table :consultors do |t|
      t.string :nome, null: false
      t.string :cpf, null: false, unique: true
      t.references :rede
      t.timestamps null: false
    end
  end
end
