require "rails_helper"

RSpec.describe ConsultorsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/consultors").to route_to("consultors#index")
    end

    it "routes to #new" do
      expect(:get => "/consultors/new").to route_to("consultors#new")
    end

    it "routes to #show" do
      expect(:get => "/consultors/1").to route_to("consultors#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/consultors/1/edit").to route_to("consultors#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/consultors").to route_to("consultors#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/consultors/1").to route_to("consultors#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/consultors/1").to route_to("consultors#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/consultors/1").to route_to("consultors#destroy", :id => "1")
    end

  end
end
