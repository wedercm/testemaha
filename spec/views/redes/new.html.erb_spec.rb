require 'rails_helper'

RSpec.describe "redes/new", type: :view do
  before(:each) do
    assign(:rede, Rede.new(
      :nome => "Rede 5"
    ))
  end

  it "renders new rede form" do
    render

    assert_select "form[action=?][method=?]", redes_path, "post" do

      assert_select "input#rede_nome[name=?]", "rede[nome]"
    end
  end
end
