  require 'rails_helper'

RSpec.describe "redes/index", type: :view do
  before(:each) do
    assign(:redes, [
      Rede.create!(
        :nome => "Rede 1"
      ),
      Rede.create!(
        :nome => "Rede 2"
      )
    ])
  end

  it "renders a list of redes" do
    render
    assert_select "Rede 1", :text => "tr>td".to_s, :count => 2
  end
end
