require 'rails_helper'

RSpec.describe "redes/edit", type: :view do
  before(:each) do
    @rede = assign(:rede, Rede.create!(
      :nome => "Rede 3"
    ))
  end

  it "renders the edit rede form" do
    render

    assert_select "form[action=?][method=?]", rede_path(@rede), "post" do

      assert_select "input#rede_nome[name=?]", "rede[nome]"
    end
  end
end
