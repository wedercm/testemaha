require 'rails_helper'

RSpec.describe "redes/show", type: :view do
  before(:each) do
    @rede = assign(:rede, Rede.create!(
      :nome => "Rede 6"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Nome/)
  end
end
