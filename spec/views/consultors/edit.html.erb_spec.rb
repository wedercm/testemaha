require 'rails_helper'

RSpec.describe "consultors/edit", type: :view do
  before(:each) do
    @consultor = assign(:consultor, Consultor.create!(:nome => "Consultor 1", :cpf => "9999"))
  end

  it "renders the edit consultor form" do
    render

    assert_select "form[action=?][method=?]", consultor_path(@consultor), "post" do
    end
  end
end
