require 'rails_helper'

RSpec.describe "consultors/new", type: :view do
  before(:each) do
    assign(:consultor, Consultor.new(:nome => "Consultor 5", :cpf => "99995"))
  end

  it "renders new consultor form" do
    render

    assert_select "form[action=?][method=?]", consultors_path, "post" do
    end
  end
end
